import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform {
  //  transform(data: any) {
  //    if (data.length > 10) {
  //      return data.substr(0, 10) + ' ...';
  //    }
  //    return data;
  //  }
  transform(data: any, limit: number) {
    if (data.length > limit) {
      return data.substr(0, limit) + ' ...';
    }
    return data;
  }
}
